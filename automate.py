# -*- coding: utf-8 -*-
from transition import *
from state import *
import os
import copy
from sp import *
from parser import *
from itertools import product
from automateBase import AutomateBase
import itertools



class Automate(AutomateBase):
        
    def succElem(self, state, lettre):
        """State x str -> list[State]
        rend la liste des états accessibles à partir d'un état
        state par l'étiquette lettre
        """
        successeurs = []
        # t: Transitions
        for t in self.getListTransitionsFrom(state):
            if t.etiquette == lettre and t.stateDest not in successeurs:
                successeurs.append(t.stateDest)
        return successeurs


    def succ (self, listStates, lettre):
        """list[State] x str -> list[State]
        rend la liste des états accessibles à partir de la liste d'états
        listStates par l'étiquette lettre
        """
        
        L=[]
        s=[]
        for e in listStates :
            	s=s+self.succElem(e,lettre)
        for i in s :	
            	if i not in L :
        		    L.append(i)
        return L
    
    
    """ Définition d'une fonction déterminant si un mot est accepté par un automate.
    Exemple :
            a=Automate.creationAutomate("monAutomate.txt")
            if Automate.accepte(a,"abc"):
                print "L'automate accepte le mot abc"
            else:
                print "L'automate n'accepte pas le mot abc"
    """
    @staticmethod
    def accepte(auto,mot) :
        """ Automate x str -> bool
        rend True si auto accepte mot, False sinon
        """

        l_init=auto.getListInitialStates()
        L_fin=auto.getListFinalStates()
        
        #lettre:str
        for lettre in mot:
            l_init=auto.succ(l_init,lettre)
        if(len(l_init)==0):
            return False
        #etat=state : int
        for state in l_init :
            	if state in L_fin :
                     return True
        return False
        
    @staticmethod
        
    def estComplet(auto,alphabet) :
        """ Automate x str -> bool
         rend True si auto est complet pour alphabet, False sinon
        """
        L= auto.listStates
        Lt= []
        
        for i in L:
          Lt=auto.getListTransitionsFrom(i)
          LettreLt=[k.etiquette for k in Lt] #Liste des etiquettes de transition à l'état "i"
          for let in alphabet:
              if let not in LettreLt:#si une lettre de l'alphabet n est pas dans la liste des etique d'une transition retourne faux
                  return False
        return True


        
    @staticmethod
    def estDeterministe(auto) :
        """ Automate  -> bool
        rend True si auto est déterministe, False sinon
        """
        #si plusieurs etat initiaux retourne faux par definition de deterministe
        if(len(auto.getListInitialStates())!=1):
            return False
        L= auto.listStates
        Lt= []
        n=0
        for i in L:
          Lt=auto.getListTransitionsFrom(i)#liste de transition depuis l'etat i
          LettreLt=[k.etiquette for k in Lt]#liste des etiquette des transition de l'etat i
          lon=len(LettreLt) #longeur de la liste (nombre de transition à l'état i)
          while n<lon-1: #compare l'etiquette n avec toutes les etiquettes qui suivent
              j=n+1
              while j<lon:
                  if LettreLt[n]==LettreLt[j]: #compare si il n'existe pas 2 transition pour la meme etiquette
                      return False
                  j=j+1    
              n=n+1
        
        return True
        

       
    @staticmethod
    def completeAutomate(auto,alphabet) :
        """ Automate x str -> Automate
        rend l'automate complété d'auto, par rapport à alphabet
        """
        
        #copie de l'automate + verification si il est deja complet
        a = copy.deepcopy(auto)
        if (Automate.estComplet(auto,alphabet)) :
            return auto
    
        #creation d'un puit 
        L= a.listStates
        puit = State(len(L)+1,False,False,"puit")
        a.addState(puit)
        for lettre in alphabet :
        	a.addTransition(Transition(puit, lettre, puit)) # ajout de toute les transitions du puit vers le puit
        for s in L:
            Lt=auto.getListTransitionsFrom(s)
            LettreLt=[k.etiquette for k in Lt]
            for lettre2 in alphabet:
            		if lettre2 not in LettreLt: #verfication + ajout si il en manque une 
            			a.addTransition(Transition(s, lettre2, puit))                    
        return a

       

    @staticmethod
    def determinisation(auto) :
        """ Automate  -> Automate
        rend l'automate déterminisé d'auto
        """
        #verification de l'automate(si il est deterministe)
        if Automate.estDeterministe(auto):
            return auto
        
        #liste des etats initiaux sans doublons
        Lis=[set(auto.getListInitialStates())]
        Lfs=[]
        Lft=[]
        cpt=0
        alphabet=auto.getAlphabetFromTransitions()
        
        #obtenir tous les états 
        for liste in Lis:
            for lettre in alphabet:
                Lsucc=auto.succ(liste,lettre)      	
                if set(Lsucc) not in Lis:
                    Lis.append(set(Lsucc))
		
        
        #création de la liste des états 		                              	
        for liste in Lis:
            label="{"
            nit=True #par defaut true
            nal=State.isFinalIn(liste)
            for etat in list(liste):	 
                label+=" "+etat.label       			        		     		
                if not etat.init: #si l'etat n'est pas inital
                    nit=False      	       		
            Lfs.append(State(cpt,nit,nal,label+"}")) #ajout à la liste 
            cpt+=1
        	       
        #création de la liste des transitions
        for j in range(0,len(Lis)):
            for lettre in alphabet:
                Lsucc=set(auto.succ(Lis[j],lettre)) #liste tempo des etats acceptant la lettre
                for i in range(0,len(Lis)):
                    if Lsucc==Lis[i]: #si il existe un etat avec la lettre
                        Lft.append(Transition(Lfs[j],lettre,Lfs[i]))
        		
        	
        #création de l'automate a partir de la liste d'etats et de transitions
        return Automate(Lft,Lfs)
        
    @staticmethod
    def complementaire(auto,alphabet):
        """ Automate -> Automate
        rend  l'automate acceptant pour langage le complémentaire du langage de a
        """
        a = Automate.completeAutomate(Automate.determinisation(copy.deepcopy(auto)),alphabet)
        Liste= a.listStates           
        for l in Liste:
            l.fin = not (l.fin)
        return a
            
              
   
    @staticmethod
    
    def intersection (auto0, auto1):
        """Automate x Automate -> Automate
        rend l'automate acceptant pour langage l'intersection des langages des deux automates
        """
        # A0 : Automate
        A0 = copy.deepcopy(auto0)
        
        # A1 : Automate
        A1 = copy.deepcopy(auto1)
        
        # LIS0 : list[set(list[State])]
        LIS0 = A0.getListInitialStates() #liste d'ensemble de liste d'etats initiaux
        
        # LIS1 : list[set(list[State])]
        LIS1 = A1.getListInitialStates() #liste d'ensemble de liste d'etats initiaux
        
        # LFSTuple : list[tuple[alpha, beta]]
        LFSTuple = list(itertools.product(LIS0, LIS1)) #liste etats finaux
        
        # LS : list[State]
        LS = []
        
        # LS0 : list[State]
        LS0 = [] 
        
        # LS1 : list[State]
        LS1 = []
        
        #LT : list[Transition]
        LT = []
        
        # cpt : int
        cpt = 0
        
        # alphabet : list[str]
        alphabet=auto1.getAlphabetFromTransitions()
        
        	
        # etat, state : State
        #création de la liste des états 
        for etat, state in LFSTuple :
        	
        	for lettre in alphabet :
        		#ajout des états accessibles
        		LS0 = A0.succElem(etat,lettre)
        		LS1 = A1.succElem(state,lettre)
        		LFS = list(itertools.product(LS0, LS1)) #produit des 2 listes d'états
        		
        
        		for l in LFS :
        			if l not in LFSTuple :
        				LFSTuple.append(l)
        				
        				
	#ajout des tuples dans la liste        					
        for etat, state in LFSTuple :
            LS.append(State(cpt, (etat.init and state.init), (etat.fin and state.fin), "("+etat.label+","+state.label+")"))
            cpt+= 1
            
            
        #création de la liste des transitions 
        for i in range((len(LFSTuple))): #i parcourt de 0 a la longueur de la liste LFSTuple
            etat, state = LFSTuple[i]
            	
            for lettre in alphabet:
            		LS0 = A0.succElem(etat,lettre)
            		LS1 = A1.succElem(state,lettre)
            		LFS = list(itertools.product(LS0, LS1))
            		
            		for l in LFS :
            			
            		
            			for j in range((len(LFSTuple))): 
            				
            				if ((LFSTuple[j] == l) and (Transition(LS[i], lettre, LS[j]) not in LT)) :
            					LT.append(Transition(LS[i],lettre,LS[j]))
	
	
	#création de l'automate         		
        return Automate(LT, LS)

    @staticmethod
    def union (auto0, auto1):
        """ Automate x Automate -> Automate
        rend l'automate acceptant pour langage l'union des langages des deux automates
        """
        a = copy.deepcopy(auto0)
        Linit0= a.getListInitialStates()
        a1 = copy.deepcopy(auto1)
        Linit1= a1.getListInitialStates()

        Liste= a1.listStates
        for s in Liste:
            a.addState(s)
            for t in a1.listTransitions:
                a.addTransition(t)
        sinit=State(0,True,False)
        a.addState(sinit)
        for l in Linit0:#parcours les etat initiaux
            LT=a.getListTransitionsFrom(l)#on fait une liste avec les transition partant de L (etat initial)
            for t in LT:# parcours les transition partant de l
                a.addTransition(Transition(sinit,t.etiquette,t.stateDest))
            l.init=False
        for l in Linit1:#parcours les etat initiaux
            LT=a.getListTransitionsFrom(l)#on fait une liste avec les transition partant de L (etat initial)
            for t in LT:# parcours les transition partant de l
                a.addTransition(Transition(sinit,t.etiquette,t.stateDest))
            l.init=False
        
        return a
        

    @staticmethod
    def concatenation (auto1, auto2): #ne marche pas bien
        """ Automate x Automate -> Automate
        rend l'automate acceptant pour langage la concaténation des langages des deux automates
        """
        
        # A : Automate
        A = copy.deepcopy(auto1)
        
        # A0 : Automate
        A0 = copy.deepcopy(auto1)
        
        # A1 : Automate
        A1 = copy.deepcopy(auto2)
        
        # cpt : int
        cpt = 0
        
        L = []
                    
        # state : State
        for s in A0.listStates :
            if(s.init == True) :
                tmp = s.id
            if(s.fin == True) :
                s.fin = False
            cpt = s.id
            
        cpt += 1
        tmp1 = cpt-1
           # etat : State    
        for etat in A1.listStates :
            etat.id = cpt
            s = State(cpt, etat.init, etat.fin, etat.label)
            A0.addState(s)
            if(etat.init == True) :
                s1 = State(cpt, etat.init, etat.fin, etat.label)
            cpt += 1
            for transition in A1.listTransitions :
                A0.addTransition(transition)
            for t in A.listTransitions :
                if(t.stateDest.id == tmp or t.stateDest.id == tmp1 and etat.init == True) :
                    A0.addTransition(Transition(t.stateSrc, t.etiquette, s1))
                    
        return A0
        
       
    @staticmethod
    def etoile (auto):
        """ Automate  -> Automate
        rend l'automate acceptant pour langage l'étoile du langage de a
        """
        a = copy.deepcopy(auto)
        Linit= a.getListInitialStates()
        Lfinal=a.getListFinalStates()
        
        for l in Linit:#parcours les etat initiaux
            LT=a.getListTransitionsFrom(l)#on fait une liste avec les transition partant de L (etat initial)
            for t in LT:# parcours les transition partant de l
                for f in Lfinal:# parcours les etat finaux
                    a.addTransition(Transition(f,t.etiquette,t.stateDest))#ajout d'une transition allant de l'etat final vers la destination d'une transition de l'etat initial de meme etiquette 
                   
        s=State(0,True,True,"1'")
        a.addState(s)
#        
                    
        return a
        
#        A= copy.deepcopy(auto)
#        
#        cpt=0
#        
#        for state in A.listStates:
#            state.id=cpt
#            cpt +=1
#            if(state.init == True):
#                for t in A.listTransitions:
#                    if(t.stateDest.fin == True):
#                        A.addTransition(Transition(t.stateSrc,t.etiquette,state))
#        s=State(cpt,True,True,"0")
#        A.addState(s)
#        
#        return A
#        
#    
