#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 10:54:20 2019

@author: 3670334
"""
from automate import *

s1 = State(1,True,False)
s2 = State(2,False,False)
s3 = State(3,False,True)
s4 = State(4,False,True)

t1 = Transition(s1,"a",s2)
t2 = Transition(s2,"b",s1)
t3 = Transition(s2,"b",s3)
t4 = Transition(s3,"a",s2)
t5 = Transition(s3,"a",s4)
t6 = Transition(s4,"b",s1)
t7 = Transition(s1,"a",s1)
t8 = Transition(s1,"b",s1)

liste_state=[s1,s2,s3,s4]
liste_trans=[t1,t2,t3,t4,t5,t6,t7,t8]

A1 = Automate(liste_trans,liste_state)

#A1.show("A1")

A4= Automate.determinisation(A1)
#A4.show("A4")

A={"a","b"}
A5 = Automate.complementaire(A1,A)
A5.show("complementaire")

ss1=State(1,True,False)
ss2=State(2,False,True)
ss3=State(3,False,True)

tt1=Transition(ss1,"b",ss1)
tt2=Transition(ss1,"a",ss2)
tt3=Transition(ss2,"b",ss1)
tt4=Transition(ss2,"b",ss2)
tt5=Transition(ss2,"a",ss3)
tt6=Transition(ss3,"b",ss1)

liste_statett=[ss1,ss2,ss3]
liste_transtt=[tt1,tt2,tt3,tt4,tt5,tt6]

A2= Automate(liste_transtt,liste_statett)

A2.show("A2")


s5=State(1,True,False)
s6=State(2,False,True)

t10 = Transition(s5,"b",s6)
t20 = Transition(s5,"a",s5)
t30 = Transition(s6,"a",s6)
t40 = Transition(s6,"b",s5)

liste_statef=[s5,s6]
liste_transf=[t10,t20,t30,t40]
A3= Automate(liste_transf,liste_statef)
#
#A3.show("A3")

A6=Automate.intersection(A2,A3)
A6.show("inter")

A7=Automate.etoile(A2)
A7.show("etoile")
