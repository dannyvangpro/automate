from automate import *

s0 = State(0,True,True)
s1 = State(1,False,False)

s2 = State(0,True,False)
s3 = State(1,False,True)

t1 = Transition(s0,"a",s0)
t2 = Transition(s0,"b",s1)
t3 = Transition(s1,"a",s1)
t4 = Transition(s1,"b",s0)


t12 = Transition(s0,"b",s0)
t22= Transition(s0,"a",s1)
t32 = Transition(s1,"b",s1)
t42 = Transition(s1,"a",s0)



liste_t= [t1,t2,t3,t4]
liste_tt= [t12,t22,t32,t42]
liste_e = [s0,s1]
liste_ee = [s2,s3]


auto1 = Automate(liste_t,liste_e)
auto2 = Automate(liste_tt, liste_ee)


#print(auto)
#print(auto1)
auto3 = Automate.intersection(auto1,auto2)
auto3.show("inter")

auto2 = Automate(liste_t,liste_e)
#auto2=Automate.creationAutomate("auto.txt")
#auto2.show("maman")


#Premiere manipulation 
#enleve la transition t
t = Transition(s0,"a",s1)
auto.removeTransition(t)
#Aucun impact car la transition n'existe pas 

#Enelve la transition t1
auto.removeTransition(t1)


#rajoute la transition t1 dans auto
auto.addTransition(t1)
#On revient au l'automate de base



#2.2.3

auto.removeState(s1)
#enleve l'etat s1


#ajouter l'etat s1

auto.addState(s1)
s2 = State(0,True,False)
auto.addState(s2)

auto1.getListTransitionsFrom(s1)

auto1.show("q2.2.2")

#Exercice de base 

print(auto.succ([s0,s1,s2],'a'))
